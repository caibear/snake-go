package main

type Segment struct {
    snake     *Snake
    direction Direction
    tile      *Tile
    previous  *Segment
    next      *Segment
}

func newSegment(snake *Snake, tile *Tile, direction Direction) *Segment {
    s := &Segment{
        snake:     snake,
        direction: direction,
        tile:      nil,
        previous:  nil,
        next:      nil,
    }
    s.move(tile.GetPos())
    return s
}

func (segment *Segment) clear() {
    if segment.tile != nil {
        if segment.tile.owner == segment {
            segment.tile.setOwner(nil)
        }
        segment.tile = nil
    }
}

func (segment *Segment) IsEmpty() bool {
    return false
}

func (segment *Segment) move(x, y int) (int, int) {
    oldX, oldY := segment.tile.GetPos()
    if segment.tile != nil && segment.tile.owner == segment {
        segment.tile.setOwner(nil)
    }

    segment.tile = segment.snake.board.tiles[x][y]
    segment.tile.setOwner(segment)

    return oldX, oldY
}

func (segment *Segment) GetTile() *Tile {
    return segment.tile
}

func (segment *Segment) IsHead() bool {
    return segment.snake.head == segment
}

func (segment *Segment) IsTail() bool {
    return segment.snake.tail == segment
}

func (segment *Segment) GetNext() *Segment {
    return segment.next
}

func (segment *Segment) GetPrevious() *Segment {
    return segment.previous
}

func (segment *Segment) GetSnake() *Snake {
    return segment.snake
}
