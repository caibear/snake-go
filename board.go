package main

import (
    "fmt"
    "log"
    "math"
    "math/rand"
    "time"
)

const boardWidth = 64
const boardHeight = 36
const startingLength = 12
const foodLength = 3
const otherFoodLength = 1

type Board struct {
    tiles       [boardWidth][boardHeight]*Tile
    entries      []Entry
    started bool
}

type Entry struct {
    snake *Snake
    operator Operator
    move Direction
    alive bool
}

func CreateBoard() *Board {
    board := &Board{entries: make([]Entry, 0, 4)}

    for i := 0; i < boardWidth; i++ {
        for j := 0; j < boardHeight; j++ {
            board.tiles[i][j] = newTile(i, j)
        }
    }

    return board
}

func (board *Board) Start() {
    if !board.started {
        board.started = true
        n := len(board.entries)

        for i, _ := range board.entries {
            x := int((float32(i) / float32(n)) * boardWidth) + int(boardWidth/(float32(n) * 2))
            s := newSnake(board, x, 0, startingLength, up)
            board.entries[i].snake = s
            board.entries[i].alive = true
        }

        for _, e := range board.entries {
            e.snake.spawnFood()
        }

        var t = time.Now()
        for active {
            if updatesPerSecond > 0 {
                delta := time.Now().Sub(t)
                wait := time.Second / updatesPerSecond - delta

                if wait > 0 {
                    time.Sleep(wait)
                } else if wait < 0 {
                    log.Printf("Board falling behind %d millis!", wait / time.Millisecond)
                }

                t = time.Now()
            }
            frame.Lock()
            board.update()
            frame.Unlock()
        }
    } else {
        panic("Board has already started!")
    }

}

func (board *Board) Get(x, y int) *Tile {
    if x < 0 || y < 0 || x >= boardWidth || y >= boardHeight {
        return nil
    } else {
        return board.tiles[x][y]
    }
}

func (board *Board) IsEmpty(x, y int) bool {
    return !(x < 0 || y < 0 || x >= boardWidth || y >= boardHeight) && board.tiles[x][y].IsEmpty()
}

func (board *Board) AddSnake(operator Operator, amount int) {
    if !board.started {
        if len(board.entries) < boardWidth {
            for i := amount; i > 0; i-- {
                board.entries = append(board.entries, Entry{operator: operator})
            }
        } else {
            panic("Board has too many snakes!")
        }
    } else {
        panic("Board has already started!")
    }
}

func (board *Board) update() {
    // Get moves
    for i, e := range board.entries {
        board.entries[i].move = e.operator.GetMove(e.snake)
    }

    // Moves snakes
    for i := 0; i < len(board.entries); i++ {
        e := board.entries[i]
        snake := e.snake
        if e.alive {
            if !snake.move(e.move) {
                fmt.Println("Snake died at length", e.snake.length)
                if snake.hasWon() {
                    size := len(board.entries) - 1
                    board.entries[i] = board.entries[size]
                    board.entries[size] = Entry{}
                    board.entries = board.entries[:size]
                    i--
                } else {
                    board.entries[i].alive = false
                    e.snake.food.clear()
                }
            }
        } else {
            if snake.length > 0 {
                snake.shrink()
            } else {
                size := len(board.entries) - 1
                board.entries[i] = board.entries[size]
                board.entries[size] = Entry{}
                board.entries = board.entries[:size]
                i--
            }
        }
    }

    // Spawn foods
    for _, e := range board.entries {
        if e.alive {
            snake := e.snake
            if snake.food.tile == nil {
                snake.spawnFood()
            }
        }
    }
}

    func (board *Board) emptyPosition() (int, int) {
        var x, y int

        for {
            x = int(rand.Float64() * boardWidth)
            y = int(rand.Float64() * boardHeight)

            if board.IsEmpty(x, y) {
                return x, y
            }
        }
    }

    func (board *Board) emptyTile() *Tile {
        x, y := board.emptyPosition()
        return board.tiles[x][y]
    }

    func (board *Board) NumSnakes() int {
        return len(board.entries)
    }

    func AddPosition(x, y, x1, y1 int) (int, int) {
        return x + x1, y + y1
    }

    func Distance(x, y, x1, y1 int) float64 {
        return math.Sqrt(float64((x - x1) * (x - x1) + (y - y1) * (y - y1)))
    }

    func Distance2(x, y, x1, y1 int) int {
        dx := x - x1
        dy := y - y1
        if dx < 0 {
        dx = -dx
    }
        if dy < 0 {
        dy = -dy
    }
        return dx + dy
    }