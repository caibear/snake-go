package main

import (
    "fmt"
    "github.com/beefsack/go-astar"
    "math/rand"
    "time"
)

type astarOperator struct {
    rand *rand.Rand
    path []astar.Pather
    pos  int
}

func newAstarOperator() *astarOperator {
    return &astarOperator{rand: rand.New(rand.NewSource(time.Now().UnixNano()))}
}

func (operator *astarOperator) GetMove(snake *Snake) Direction {
    if operator.path == nil || operator.pos == 0 {
        path, _, found := astar.Path(snake.GetHead().GetTile(), snake.GetFood().GetTile())

        if found {
            operator.path = path
            operator.pos = len(path) - 1
        } else {
            fmt.Println("Random direction")
            return randomDirection(operator.rand)
        }
    }

    current := operator.path[operator.pos].(*Tile)
    next := operator.path[operator.pos - 1].(*Tile)
    operator.pos--
    dir := current.Direction(next)

    return dir
}